//
//  DCGMScanViewController.m
//  CDDStoreDemo
//
//  Created by 陈甸甸 on 2018/1/9.
//Copyright © 2018年 RocketsChen. All rights reserved.
//

#import "DCGMScanViewController.h"

#import <AVFoundation/AVFoundation.h>

#import "DCCameraTopView.h"
#import "DCScanIdentifyAreaView.h"

#define WEAKSELF __weak typeof(self) weakSelf = self;
/** 屏幕高度 */
#define ScreenH [UIScreen mainScreen].bounds.size.height
/** 屏幕宽度 */
#define ScreenW [UIScreen mainScreen].bounds.size.width

//屏幕高度 宽度
#define DCScreenH \
([[UIScreen mainScreen] respondsToSelector:@selector(nativeBounds)] ? [UIScreen mainScreen].nativeBounds.size.height/[UIScreen mainScreen].nativeScale : [UIScreen mainScreen].bounds.size.height)

#define DCScreenW \
([[UIScreen mainScreen] respondsToSelector:@selector(nativeBounds)] ? [UIScreen mainScreen].nativeBounds.size.width/[UIScreen mainScreen].nativeScale : [UIScreen mainScreen].bounds.size.width)


///, DCScanBackDelegate
@interface DCGMScanViewController () <AVCaptureMetadataOutputObjectsDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AVCaptureVideoDataOutputSampleBufferDelegate>

/* 用来捕捉管理活动的对象 */
@property (strong,nonatomic)AVCaptureSession *session;
/* 设备 */
@property (strong,nonatomic)AVCaptureDevice *device;
/* 捕获输入 */
@property (strong,nonatomic)AVCaptureDeviceInput *input;
/* 捕获输出 */
@property (strong,nonatomic)AVCaptureMetadataOutput *output;
/* 背景 */
@property (strong,nonatomic)AVCaptureVideoPreviewLayer *ffView;
/* 输出流 */
@property (nonatomic, strong) AVCaptureVideoDataOutput *videoDataOutput;
/* 自定义识别区View */
@property (strong , nonatomic)DCScanIdentifyAreaView *areaView;


/* 顶部工具View */
@property (nonatomic, strong) DCCameraTopView *cameraTopView;

/* 闪光灯按钮 */
@property (strong , nonatomic)UIButton *flashButton;

@end

@implementation DCGMScanViewController

#pragma mark - LazyLoad
- (UIButton *)flashButton
{
    if (!_flashButton) {
        _flashButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _flashButton.alpha = 0;
        [_flashButton addTarget:self action:@selector(flashButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _flashButton;
}


#pragma mark - LifeCyle


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpTopView];
    [self setupCamera];
}

#pragma mark - 导航栏处理
- (void)setUpTopView
{
    _cameraTopView = [[DCCameraTopView alloc] initWithFrame:CGRectMake(0, 0, ScreenW, 64)];
    WEAKSELF
    _cameraTopView.leftItemClickBlock = ^{
//        [weakSelf.navigationController popViewControllerAnimated:YES];
        [weakSelf dismissViewControllerAnimated:YES completion:nil];
    };
    
    _cameraTopView.rightItemClickBlock = ^{
        [weakSelf flashButtonClick:weakSelf.flashButton];
    };
    
    _cameraTopView.rightRItemClickBlock = ^{
        [weakSelf jumpPhotoAlbum];
    };
    
    [self.view addSubview:_cameraTopView];
}

- (void)flashButtonClick:(UIButton *)button
{
    if (button.selected == NO) {
        [self openFlashlight];
    } else {
        [self closeFlashlight];
    }
    
    button.selected = !button.selected;
}


#pragma mark - 打开手电筒
- (void)openFlashlight {
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    if ([captureDevice hasTorch]) {
        BOOL locked = [captureDevice lockForConfiguration:&error];
        if (locked) {
            captureDevice.torchMode = AVCaptureTorchModeOn;
            [captureDevice unlockForConfiguration];
        }
    }
}

#pragma mark - 关闭手电筒
- (void)closeFlashlight {
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if ([device hasTorch]) {
        [device lockForConfiguration:nil];
        [device setTorchMode: AVCaptureTorchModeOff];
        [device unlockForConfiguration];
    }
}

- (void)jumpPhotoAlbum
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        UIAlertController *alter = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"暂无权限" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        }];
        [alter addAction:okAction];
        
        [self presentViewController:alter animated:YES completion:nil];
        
        return;
    }
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    [picker.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbarScanBg"] forBarMetrics:UIBarMetricsDefault];
    picker.view.backgroundColor = [UIColor whiteColor];
    picker.delegate = self;
    
    [self showDetailViewController:picker sender:nil];
}

#pragma mark - <初始化相机设备等扫描控件>
- (void)setupCamera
{
    // Device
    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    __weak typeof(self)weakSelf = self;
    [weakSelf setUpJudgmentWithScuessBlock:^{
        
        [self setUpPutInit]; //初始化
        
        [self setUpFullFigureView]; //背景面
        
        [self.session startRunning]; //开启扫描
        
    }]; //设备权限判断
}

#pragma mark - 扫描区域
- (void)setUpPutInit
{
    //初始化
    _input = [AVCaptureDeviceInput deviceInputWithDevice:self.device error:nil];
    
    _output = [AVCaptureMetadataOutput new];
    [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    
    //设备输出流
    self.videoDataOutput = [[AVCaptureVideoDataOutput alloc] init];
    [_videoDataOutput setSampleBufferDelegate:self queue:dispatch_get_main_queue()];
    
    _session = [AVCaptureSession new];
    [_session addOutput:_videoDataOutput]; //添加到sesson，识别光线强弱
    
    //限制扫描区域
    CGSize size = self.view.bounds.size;
    CGRect cropRect = CGRectMake(DCScreenW * 0.1, DCScreenW * 0.3, DCScreenW * 0.8, DCScreenW * 0.8);
    CGFloat p1 = size.height/size.width;
    CGFloat p2 = 1920./1080.;  //使用了1080p的图像输出
    if (p1 < p2) {
        CGFloat fixHeight = DCScreenW * 1920. / 1080.;
        CGFloat fixPadding = (fixHeight - size.height)/2;
        _output.rectOfInterest = CGRectMake((cropRect.origin.y + fixPadding)/fixHeight,cropRect.origin.x/size.width,cropRect.size.height/fixHeight,cropRect.size.width/size.width);
    }else{
        CGFloat fixWidth = self.view.frame.size.height * 1080. / 1920.;
        CGFloat fixPadding = (fixWidth - size.width)/2;
        _output.rectOfInterest = CGRectMake(cropRect.origin.y/size.height,(cropRect.origin.x + fixPadding)/fixWidth,cropRect.size.height/size.height,cropRect.size.width/fixWidth);
    }
    
    
    //设置检测质量，质量越高扫描越精确，默认AVCaptureSessionPresetHigh
    if ([_device supportsAVCaptureSessionPreset:AVCaptureSessionPreset1920x1080]) {
        if ([_session canSetSessionPreset:AVCaptureSessionPreset1920x1080]) {
            [_session setSessionPreset:AVCaptureSessionPreset1920x1080];
        }
    }else if ([_device supportsAVCaptureSessionPreset:AVCaptureSessionPreset1280x720]) {
        if ([_session canSetSessionPreset:AVCaptureSessionPreset1280x720]) {
            [_session setSessionPreset:AVCaptureSessionPreset1280x720];
        }
    }
    
    //捕捉
    [_session setSessionPreset:AVCaptureSessionPresetHigh];
    if ([_session canAddInput:self.input]){
        [_session addInput:self.input];
    }
    
    if ([_session canAddOutput:self.output]){
        [_session addOutput:self.output];
    }
    
    
    // 扫码类型
    [self.output setMetadataObjectTypes:[NSArray arrayWithObjects:AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode128Code, AVMetadataObjectTypeQRCode, nil]];
}


#pragma mark - 全系背景View
- (void)setUpFullFigureView
{
    
    _ffView = [AVCaptureVideoPreviewLayer layerWithSession:self.session];
    _ffView.videoGravity = AVLayerVideoGravityResizeAspectFill;
    _ffView.frame = CGRectMake(0,0,DCScreenW,self.view.frame.size.height);
    [self.view.layer insertSublayer:self.ffView atIndex:0];
    
    _areaView = [[DCScanIdentifyAreaView alloc] initWithFrame:_ffView.bounds];
    _areaView.scanFrame = CGRectMake(DCScreenW * 0.1, DCScreenW * 0.3, DCScreenW * 0.8, DCScreenW * 0.8);
    [_ffView addSublayer:_areaView.layer];
    
    
    [self.view addSubview:self.flashButton];
    self.flashButton.frame = CGRectMake((DCScreenW - 25) * 0.5 , DCScreenW * 1.1 - 50, 20, 40); //手电筒的尺寸
}

#pragma mark - 弹框
- (void)setUpAlterViewWith:(UIViewController *)currVc WithReadContent:(NSString *)content WithLeftMsg:(NSString *)leftMsg LeftBlock:(dispatch_block_t)leftClickBlock RightMsg:(NSString *)rightMsg RightBliock:(dispatch_block_t)rightClickBlock
{
    UIAlertController *alter = [UIAlertController alertControllerWithTitle:@"温馨小提示" message:content preferredStyle:UIAlertControllerStyleAlert];
    
    if (leftMsg.length != 0) {
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:leftMsg style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            !leftClickBlock ? : leftClickBlock();
        }];
        [alter addAction:okAction];
        
    }
    
    if (rightMsg.length != 0) {
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:rightMsg style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            !rightClickBlock ? : rightClickBlock();
        }];
        [alter addAction:okAction];
        
    }
    
    [currVc presentViewController:alter animated:YES completion:nil];
}


#pragma mark - 设备权限判断
- (void)setUpJudgmentWithScuessBlock:(dispatch_block_t)openSession
{
    //权限
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if(authStatus == AVAuthorizationStatusRestricted || authStatus == AVAuthorizationStatusDenied){
        
        [self setUpAlterViewWith:self WithReadContent:
					@"打开手机相册" WithLeftMsg:@"我知道了" LeftBlock:nil RightMsg:@"现在就去" RightBliock:^{
            NSURL *qxUrl = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            
            if([[UIApplication sharedApplication] canOpenURL:qxUrl]) { //跳转到本应用APP的权限界面
                
                NSURL*url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                [[UIApplication sharedApplication] openURL:url];
                
            }
        }];
        
    }else if(_device == nil){ //未找到设备
        
		[self setUpAlterViewWith:self WithReadContent:@"温馨小提示 未找到设备 " WithLeftMsg:@"确定" LeftBlock:nil RightMsg:nil RightBliock:nil];
        
    }else{ //识别到设备以及打开权限成功后回调
        
        !openSession ? : openSession(); //回调
    }
}

#pragma mark - <AVCaptureMetadataOutputObjectsDelegate>
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    
    if ([metadataObjects count] >0){
        
        AVMetadataMachineReadableCodeObject *metadataObject = [metadataObjects objectAtIndex:0];
        NSLog(@"扫描结果：%@",metadataObject.stringValue);
        if (metadataObject.stringValue.length != 0) {
            [self DCScanningSucessBackWithInfor:metadataObject.stringValue];
        }
    }
}

#pragma mark - 调整图片尺寸
- (UIImage *)resizeImage:(UIImage *)image WithMaxSize:(CGSize)maxSize{
    
    if (image.size.width < maxSize.width && image.size.height < maxSize.height) {
        return image;
    }
    maxSize = (maxSize.width == 0)  ?  CGSizeMake(1000, 1000) : maxSize;
    CGFloat xScale = maxSize.width / image.size.width;
    CGFloat yScale = maxSize.height / image.size.height;
    CGFloat scale = MIN(xScale, yScale);
    CGSize size = CGSizeMake(image.size.width * scale, image.size.height * scale);
    
    UIGraphicsBeginImageContext(size);
    
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return result;
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage *image = [self resizeImage:info[UIImagePickerControllerOriginalImage] WithMaxSize:CGSizeMake(1000, 1000)];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{ //异步
        
        CIDetector *detector = [CIDetector detectorOfType:CIDetectorTypeQRCode context:nil options:@{CIDetectorAccuracy : CIDetectorAccuracyHigh}];
        
        CIImage *selImage = [[CIImage alloc] initWithImage:image];
        NSArray *features = [detector featuresInImage:selImage];
        
        NSMutableArray *arrayM = [NSMutableArray arrayWithCapacity:features.count];
        for (CIQRCodeFeature *feature in features) {
            [arrayM addObject:feature.messageString];
        }
        __weak typeof(self)weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (arrayM.copy != nil && ![arrayM isKindOfClass:[NSNull class]] && arrayM.count != 0) {
                [weakSelf dismissViewControllerAnimated:YES completion:nil];
                 [weakSelf DCScanningSucessBackWithInfor:arrayM.firstObject];
//                [self setUpAlterViewWith:self WithReadContent:[NSString stringWithFormat:@"扫描结果为：%@",arrayM.copy] WithLeftMsg:@"好的" LeftBlock:^{
//                    [weakSelf DCScanningSucessBackWithInfor:arrayM.copy];
//                    [weakSelf stopDeviceScanning]; //停止扫描
//                } RightMsg:nil RightBliock:nil];
            }else{
                [weakSelf setUpAlterViewWith:self WithReadContent:@"温馨提示" WithLeftMsg:@"确定" LeftBlock:^{
                    [weakSelf dismissViewControllerAnimated:YES completion:nil];
                } RightMsg:nil RightBliock:nil];
            }
        });
    });
}







#pragma mark - 停止扫描
- (void)stopDeviceScanning
{
    [_session stopRunning];
    _session = nil;
}


#pragma mark - <AVCaptureVideoDataOutputSampleBufferDelegate>
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
    
    // 内存稳定调用这个方法的时候
    CFDictionaryRef metadataDict = CMCopyDictionaryOfAttachments(NULL,sampleBuffer, kCMAttachmentMode_ShouldPropagate);
    NSDictionary *metadata = [[NSMutableDictionary alloc] initWithDictionary:(__bridge NSDictionary*)metadataDict];
    CFRelease(metadataDict);
    NSDictionary *exifMetadata = [[metadata objectForKey:(NSString *)kCGImagePropertyExifDictionary] mutableCopy];
    float brightnessValue = [[exifMetadata objectForKey:(NSString *)kCGImagePropertyExifBrightnessValue] floatValue]; //光线强弱度
    
    if (!self.flashButton.selected) {
        self.flashButton.alpha =  (brightnessValue < 1.0) ? 1 : 0;
    }
}

- (void)dealloc {
    [self stopDeviceScanning]; //停止扫描
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [_session startRunning]; //开始扫描

    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_session stopRunning]; //停止扫描

    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

#pragma mark - <DCScanBackDelegate>
- (void)DCScanningSucessBackWithInfor:(NSString *)message {
	if (self.delegate && [self.delegate respondsToSelector:@selector(scanViewController:scanString:)]) {
		[self.delegate scanViewController:self scanString:message];
	}
	return;
    if (!([message hasPrefix:@"http://"] || [message hasPrefix:@"https://"])) {
        __weak typeof(self)weakSelf = self;
		[self setUpAlterViewWith:self WithReadContent:[NSString stringWithFormat:@"扫描结果：%@",message] WithLeftMsg:@"确定" LeftBlock:^{
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
        } RightMsg:nil RightBliock:^{
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", message]]];
//            return ;
//            WebViewController *vc = [WebViewController new];
//            vc.urlString = message;
//            [weakSelf presentViewController:vc animated:YES completion:nil];
        }];
    } else {
		[self dismissViewControllerAnimated:YES completion:^{
			[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", message]]];
		}];
	}
}

@end
