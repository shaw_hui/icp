//
//  BlackTipView.m
//  龙凤
//
//  Created by mac on 15/4/24.
//  Copyright (c) 2015年 air. All rights reserved.
//

#import "BlackTipView.h"

@implementation BlackTipView

+ (void)showBlackTipView:(UIView *)view TipName:(NSString *)tipName {

	CGFloat RE_ScreenWidth = [UIScreen mainScreen].bounds.size.width;
	CGFloat RE_ScreenHeight = [UIScreen mainScreen].bounds.size.height;

	
    UIView *successTipView = [[UIView alloc] initWithFrame:CGRectMake((RE_ScreenWidth-140)*0.5, RE_ScreenHeight - 140, 140, 35)];
    successTipView.backgroundColor = [UIColor blackColor];
    successTipView.alpha = 1.0;
    [view addSubview:successTipView];
    UILabel *successfulLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, 120, 30)];
    successfulLabel.text = tipName;           //@"此商品购物车已添加"
    successfulLabel.textAlignment = NSTextAlignmentCenter;
    successfulLabel.textColor = [UIColor whiteColor];
    successfulLabel.backgroundColor = [UIColor clearColor];
    successfulLabel.numberOfLines = 0;
    successfulLabel.font = [UIFont boldSystemFontOfSize:12.0];
    [successTipView addSubview:successfulLabel];
    [UIView animateWithDuration:1.5 animations:^{
        successTipView.alpha = 0.8;
    } completion:^(BOOL finished) {
        
        [successfulLabel removeFromSuperview];
        [successTipView removeFromSuperview];
    }];
  
}

+ (void)showBlackTipView:(UIView *)view TipName:(NSString *)tipName frame:(CGRect)frame  {
    UIView *successTipView = [[UIView alloc] initWithFrame:frame];
    successTipView.backgroundColor = [UIColor blackColor];
    successTipView.alpha = 1.0;
    [view addSubview:successTipView];
    UILabel *successfulLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, 120, 30)];
    successfulLabel.text = tipName;           //@"此商品购物车已添加"
    successfulLabel.textAlignment = NSTextAlignmentCenter;
    successfulLabel.textColor = [UIColor whiteColor];
    successfulLabel.backgroundColor = [UIColor clearColor];
    successfulLabel.numberOfLines = 0;
    successfulLabel.font = [UIFont boldSystemFontOfSize:12.0];
    [successTipView addSubview:successfulLabel];
    [UIView animateWithDuration:1.5 animations:^{
        successTipView.alpha = 0.8;
    } completion:^(BOOL finished) {
        
        [successfulLabel removeFromSuperview];
        [successTipView removeFromSuperview];
    }];
}

@end
