//
//  GoodDetailController.h
//  JXBWebKitProject
//
//  Created by 金修博 on 2018/8/4.
//  Copyright © 2018年 金修博. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GoodDetailControllerDelegate <NSObject>

- (void)goodDetailControllerDidselectTest;

@end

@interface GoodDetailController : UIViewController
@property (nonatomic, weak) id<GoodDetailControllerDelegate> delegate;
@end
