//
//  BlackTipView.h
//  龙凤
//
//  Created by mac on 15/4/24.
//  Copyright (c) 2015年 air. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlackTipView : NSObject
/*
 * @param view为要显示的提示的父view
 * @param tipName 为要显示的字符串
 */
+ (void)showBlackTipView:(UIView *)view TipName:(NSString *)tipName ;

+ (void)showBlackTipView:(UIView *)view TipName:(NSString *)tipName frame:(CGRect)frame ;

@end
