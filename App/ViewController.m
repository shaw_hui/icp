//
//  ViewController.m
//  App
//
//  Created by feidu on 2019/1/9.
//  Copyright © 2019 refuse. All rights reserved.
//

#import "ViewController.h"
#import "DCGMScanViewController.h"
#import <CommonCrypto/CommonCrypto.h>

@interface ViewController () <DCGMScanViewControllerDelegate, JXBWebViewControllerDelegate>

@end

@implementation ViewController

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	self.navigationController.navigationBar.hidden = YES;
}
- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	self.navigationController.navigationBar.hidden = YES;
}


- (instancetype)init {
	if (self = [super init]) {
		self.allowsBFNavigationGesture = YES;
		self.progressTintColor = [UIColor blackColor];
//		MyViewController *webVC = [[MyViewController alloc] initWithURLString:model.url];
//		[self.navigationController pushViewController:webVC animated:YES];
		NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:KURL]];
		[self.webView loadRequest:request];
		self.delegate = self;

	}
	return self;
}


- (void)viewDidLoad {
	[super viewDidLoad];
	[self addNotificationCenter];
	
}

- (void)addNotificationCenter {
	/// 打开扫描
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(open_scan) name:@"open_scan" object:nil];

}

- (void)open_scan {
	dispatch_async(dispatch_get_main_queue(), ^{
		[self openScan];
	});
}

- (void)openScan {
	NSLog(@"jfkldjsalfkjaf");
	DCGMScanViewController *vc = [DCGMScanViewController new];
	vc.delegate = self;
//	[self presentViewController:vc animated:YES completion:nil];
	[self.navigationController pushViewController:vc animated:YES];
}


- (void)scanViewController:(DCGMScanViewController *)controller scanString:(NSString *)message {
	[controller dismissViewControllerAnimated:YES completion:nil];
	
	if (!([message hasPrefix:@"http://"] || [message hasPrefix:@"https://"])) {
		
		//		__weak typeof(self)weakSelf = self;
		//		[self setUpAlterViewWith:self WithReadContent:[NSString stringWithFormat:@"扫描结果：%@",message] WithLeftMsg:@"好的" LeftBlock:^{
		//			// [weakSelf dismissViewControllerAnimated:YES completion:nil];
		//		} RightMsg:nil RightBliock:^{
		//			// [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", message]]];
		//			// return ;
		//			// WebViewController *vc = [WebViewController new];
		//			// vc.urlString = message;
		//			// [weakSelf presentViewController:vc animated:YES completion:nil];
		//		}];
	} else {
		NSURL *url = [NSURL URLWithString:message];
		NSURLRequest *request = [NSURLRequest requestWithURL:url];
		[self.webView loadRequest:request];
		return;
		[self dismissViewControllerAnimated:YES completion:^{
			[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", message]]];
		}];
	}
}


- (void)setUserId:(NSString *)userid {
	[UserDefaultInfo setUserId:userid];
}

- (NSString *)md5:(NSString *)str {
	const char *cStr = [str UTF8String];
	unsigned char digest[CC_MD5_DIGEST_LENGTH];
	CC_MD5(cStr,(CC_LONG)strlen(cStr), digest);
	NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
	for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
		[output appendFormat:@"%02x", digest[i]];
	return  output;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}


#pragma mark - WKNavigationDelegate
- (NSInteger)typeSelectWebView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction {
	NSInteger type = [super typeSelectWebView:webView decidePolicyForNavigationAction:navigationAction];
	
	return type;
}

- (void)interceptRequestWithNavigationAction:(WKNavigationAction *)navigationAction
							 decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
	decisionHandler(WKNavigationActionPolicyCancel);
	NSLog(@"1flkdsjlfkjsklf");
	
}

//在收到响应后，决定是否跳转(表示当客户端收到服务器的响应头，根据response相关信息，可以决定这次跳转是否可以继续进行。)
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler {
	
	decisionHandler(WKNavigationResponsePolicyAllow);
}

//接收到服务器跳转请求之后调用(接收服务器重定向时)
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(null_unspecified WKNavigation *)navigation {
	
}

//当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(null_unspecified WKNavigation *)navigation {
	
}



- (void)webViewControllerDidStartLoad:(JXBWebViewController *)webViewController {

	NSLog(@"%@", webViewController.webView.URL.absoluteString);
}

- (void)webViewControllerDidFinishLoad:(JXBWebViewController *)webViewController {
	// 完成
}
- (void)webViewController:(JXBWebViewController *)webViewController didFailLoadWithError:(NSError *)error {
	// 错误
}


@end
