//
//  AppDelegate+util.h
//  WebViewController
//
//  Created by feidu_ios_02 on 2018/11/16.
//  Copyright © 2018 feidu. All rights reserved.
//

#import "AppDelegate.h"


//#import <AlipaySDK/AlipaySDK.h>

// 引导页
//#import "JYJPromotionPageViewController.h"
//#import "KSGuaidViewManager.h"

///极光推送
////#import "JPUSHService.h" // 引入JPush功能所需头文件
//#ifdef NSFoundationVersionNumber_iOS_9_x_Max
//	// iOS10注册APNs所需头文件
//	#import <UserNotifications/UserNotifications.h>
//#endif
//
//// 如果需要使用idfa功能所需要引入的头文件（可选）
//#import <AdSupport/AdSupport.h>

NS_ASSUME_NONNULL_BEGIN

@interface AppDelegate (util)

@end

NS_ASSUME_NONNULL_END
