//
//  UserDefaultInfo.m
//  mingpin90
//
//  Created by feidu_ios_02 on 2018/3/27.
//  Copyright © 2018年 feidu. All rights reserved.
//

#import "UserDefaultInfo.h"
//#import "JPUSHService.h"
#import <UIKit/UIKit.h>

@interface UserDefaultInfo ()
@property (nonatomic, strong) NSArray  *titlesArray;
@property (nonatomic, copy)   NSString *deviceString;
@property (nonatomic, copy)   NSString *userID;
@property (nonatomic, copy)   NSString *langerType;
@property (nonatomic, copy)   NSArray  *cookieArray;
@property (nonatomic, copy)   NSArray  *cookieSession;
@property (nonatomic, copy)   NSArray  *cookieUserId;
@property (nonatomic, copy)   NSString  *token;
@property (nonatomic, copy)   NSString  *deviceToken;

@property (nonatomic, copy)   NSString  *myUserId;

@property (nonatomic, copy)   NSArray  *userInfoArray;


@property (nonatomic, copy)   NSString  *qiniuyunToken;

@end

@implementation UserDefaultInfo
static UserDefaultInfo *userDefaultInfo = nil;

+ (UserDefaultInfo *)defaultManager {
	if ( userDefaultInfo == nil ) {
		@synchronized(self) {
			if ( userDefaultInfo == nil ) {
				userDefaultInfo = [[[self class] alloc] init];
			}
		}
	}
	return userDefaultInfo;
}


///   当前时间

- (NSTimeInterval)getCurrentTimer {
	NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];	//获取当前时间0秒后的时间
	NSTimeInterval time = [date timeIntervalSince1970];		// 精确到秒
	return time;
//	NSString *timeString = [NSString stringWithFormat:@"%.0f", time];
	
//	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//	[defaults setObject:timeString forKey:@"endingTimerString"];
//	[defaults synchronize];
}

- (void)setLingHongbaoTime {
	NSTimeInterval time = [self getCurrentTimer];
	NSString *timeString = [NSString stringWithFormat:@"%.0f", time];
	
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:timeString forKey:@"endingTimerString"];
	[defaults synchronize];
}

- (void)resetTimer {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:nil forKey:@"endingTimerString"];
	[defaults synchronize];

}


- (BOOL)isCanLingHongbaoTime {
	NSString *oldTimer = [[NSUserDefaults standardUserDefaults] objectForKey:@"endingTimerString"];
	if (oldTimer == nil) return YES;

	NSTimeInterval time = [self getCurrentTimer];
	NSTimeInterval oldTime = [oldTimer doubleValue];
	return (time - oldTime) > (60 * 10);
}

- (int)LingHongbaoTime {
	NSTimeInterval time = [self getCurrentTimer];
	NSTimeInterval oldTime = [[[NSUserDefaults standardUserDefaults] objectForKey:@"endingTimerString"] doubleValue];
	return (int)((60 * 10) - (time - oldTime));
}


- (void)setTitlsDataArray:(NSArray *)dataArray {
	userDefaultInfo.titlesArray = dataArray;
}

- (NSArray *)getTitlsDataArray {
	return userDefaultInfo.titlesArray;
}



- (void)setDeviceStringByString:(NSString *)deviceString {
	
	
	
	userDefaultInfo.deviceString = deviceString;
}

- (NSString *)getTitlsDeviceString {
	return userDefaultInfo.deviceString;
}


+ (instancetype)allocWithZone:(NSZone *)zone {
	if ( userDefaultInfo == nil ) {
		userDefaultInfo = [super allocWithZone:zone];
	}
	return userDefaultInfo;
}
- (instancetype)copyWithZone:(NSZone *)zone {
	return self;
}





// 判断是否已经登录了
+ (BOOL)isLoging {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	if ( [defaults objectForKey:@"logningToken"] ) {
		return YES;
	}
	return NO;
}


// 购物车
+ (void)setCarCount:(NSString *)count {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:count forKey:@"logningMobilecount"];
	[[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)getCarCount {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSString *countString = [defaults objectForKey:@"logningMobilecount"];
	if (countString && countString.integerValue) {
		return countString;
	}
	return nil;
}


// 登录手机号
+ (void)setLoingMobile:(NSString *)mobile {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:mobile forKey:@"logningMobile"];
	[[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)getLoingMobile {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	return [defaults objectForKey:@"logningMobile"];
}


- (void)setImageCookie:(NSArray *)array {
	self.cookieArray = [NSArray arrayWithArray:array];
	NSHTTPCookie *cookie;
	for(id c in self.cookieArray) {
		if([c isKindOfClass:[NSHTTPCookie class]]) {
			cookie = (NSHTTPCookie*)c;
			if([cookie.name isEqualToString:@"PHPSESSID"]) {
				self.cookieSession = cookie.value;
			}
			
			if([cookie.name isEqualToString:@"COOKIEW"]) {
				self.cookieUserId = cookie.value;
			}
		}
	}
	
}

- (NSArray *)getImageCookie{
	return self.cookieArray ;
}
// 登录token
+ (void)setLoingValueForKey:(NSString *)token {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:token forKey:@"logningToken"];
	[[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)getLoingValuetoken {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	return [defaults objectForKey:@"logningToken"];
}

// 退出登录
+ (void)loingout {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	[defaults setObject:nil forKey:@"logningMobile"];
	[defaults setObject:nil forKey:@"logningToken"];
	[defaults setObject:nil forKey:@"logningMobilecount"];
	
	//#warning 删除别名
	//    // 设置标签和(或)别名（若参数为nil，则忽略；若是空对象，则清空；详情请参考文档：https://docs.jiguang.cn/jpush/client/iOS/ios_api/）
	//    NSSet *set = [NSSet set];
	//    NSString *string = [NSString string];
	//    [JPUSHService setTags:set alias:string fetchCompletionHandle:^(int iResCode, NSSet *iTags, NSString *iAlias) {}];
	//
	//    [JPUSHService setAlias:string callbackSelector:@selector(setAliasBack:) object:nil];
	//    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void)setAliasBack:(id)send {
	
}


+ (void)setLangerType:(NSString *)langerType {
	[self defaultManager].langerType = langerType;
}
+ (NSString *)getLangerType {
	return [self defaultManager].langerType ;
}

+ (void)setUserId:(NSString *)userId {
	[self defaultManager].userID = userId;
}
+ (NSString *)getUserId {
	return [self defaultManager].userID ;
}

- (NSString *)getRequestCookie {
	if (self.cookieArray && self.cookieArray.count) {
		NSMutableString *cookie = [NSMutableString string];
		[cookie appendFormat:@"%@-%@", self.cookieSession, self.cookieUserId];
		
		return cookie;
	}
	return @"";
}

- (void)setToken:(NSString *)token {
	_token = token;
}
- (NSString *)getToken {
	return _token;
}


- (void)setDeviceToken:(NSString *)deviceToken {
	_deviceToken = deviceToken;
	
}
- (NSString *)getDeviceToken {
	return _deviceToken;
}

- (void)setUserInfoDataSourse:(NSArray *)dataArray {
	self.userInfoArray = [NSMutableArray arrayWithArray:dataArray];
}
- (NSArray *)getUserInfoDataSourse {
	return self.userInfoArray;
}
- (void)setQinniuToken:(NSString *)token {
	self.qiniuyunToken = token;
}
- (NSString *)getQinniuToken {
	return self.qiniuyunToken;
}
@end
