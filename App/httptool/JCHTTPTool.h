//
//  LoadData.h
//  龙凤
//
//  Created by mac on 15/4/23.
//  Copyright (c) 2015年 air. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "GWAlertView.h"

@interface JCHTTPTool : NSObject

+ (void)postWithUrl:(NSString *_Nonnull)url
             params:(NSDictionary *_Nullable)params
             header:(NSDictionary *_Nullable)header
            success:(void(^_Nullable)(id _Nonnull responseObject))success
            failure:(void(^_Nullable)(NSError * _Nonnull error))failure;

+ (void) getWithUrl:(NSString *_Nullable)url
             params:(NSDictionary *_Nullable)params
            success:(void(^_Nullable)(id _Nonnull json))success
            failure:(void(^_Nullable)(NSError * _Nonnull error))failure;

+ (void) postWithUrl:(NSString *_Nullable)url
              params:(NSDictionary *_Nullable)params
             success:(void(^_Nullable)(id _Nonnull responseObject))success
             failure:(void(^_Nullable)(NSError * _Nonnull error))failure;

+ (NSString *_Nullable) paramValueOfUrl:(NSString *_Nullable)url
                              withParam:(NSString *_Nullable) param;

//从html字串中提取img url
+ (NSArray *_Nullable)filterImage:(NSString *_Nullable)html;
+ (CGSize)getImageSizeWithURL:(id _Nullable )imageURL;

+ (void)postThirdWithUrl:(NSString *_Nullable)url
                  params:(NSDictionary *_Nullable)params
                progress:(void(^_Nullable)(NSProgress * _Nonnull uploadProgress))progress
                 success:(void(^_Nullable)(id _Nullable responseObject))success
                 failure:(void(^_Nullable)(NSError * _Nullable error))failure;

@end
