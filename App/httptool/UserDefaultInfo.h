//
//  UserDefaultInfo.h
//  mingpin90
//
//  Created by feidu_ios_02 on 2018/3/27.
//  Copyright © 2018年 feidu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BlackTipView.h"

@interface UserDefaultInfo : NSObject
+ (UserDefaultInfo *)defaultManager ;

// 红包领取时间间隔
- (void)setLingHongbaoTime;
- (void)resetTimer;
- (BOOL)isCanLingHongbaoTime;
- (int)LingHongbaoTime;

- (void)setDeviceToken:(NSString *)deviceToken;
- (NSString *)getDeviceToken;


- (void)setToken:(NSString *)token;
- (NSString *)getToken;

- (void)setDeviceStringByString:(NSString *)deviceString;
- (NSString *)getTitlsDeviceString;


- (void)setTitlsDataArray:(NSArray *)dataArray;
- (NSArray *)getTitlsDataArray ;

// 登录手机号
+ (void)setLoingMobile:(NSString *)mobile;
+ (NSString *)getLoingMobile;
// 登录token
+ (void)setLoingValueForKey:(NSString *)token ;
+ (NSString *)getLoingValuetoken ;
+ (BOOL)isLoging ;
+ (void)loingout;

+ (void)setCarCount:(NSString *)count;
+ (NSString *)getCarCount;

+ (void)setUserId:(NSString *)userId;
+ (NSString *)getUserId;
+ (void)setLangerType:(NSString *)langerType;
+ (NSString *)getLangerType;



- (void)setImageCookie:(NSArray *)array ;
- (NSArray *)getImageCookie;

- (NSString *)getRequestCookie;

- (void)setUserInfoDataSourse:(NSArray *)dataArray;
- (NSArray *)getUserInfoDataSourse;

- (void)setQinniuToken:(NSString *)token;
- (NSString *)getQinniuToken;

@end
