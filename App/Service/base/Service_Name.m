//
//  Service_Name.m
//  JXBWebKitProject
//
//  Created by 金修博 on 2018/8/4.
//  Copyright © 2018年 金修博. All rights reserved.
//

#import "Service_Name.h"
#import "GoodListController.h"
#import "GoodDetailController.h"
#import "WKAppManager.h"

@implementation Service_Name

- (void)func_list:(NSDictionary *)params {
	NSLog(@"%s", __FUNCTION__);
    GoodListController *vc = [[GoodListController alloc] init];
    vc.params = params[@"data"];
    vc.successCallback = params[@"success"];
    vc.failCallback = params[@"fail"];
    vc.progressCallback = params[@"progress"];
    [[WKAppManager sharedInstance].currentNavigationController pushViewController:vc animated:YES];
}

- (void)func_detail:(id)params {
	NSLog(@"%s", __FUNCTION__);
	NSLog(@"%@", params);
    GoodDetailController *vc = [[GoodDetailController alloc] init];
	vc.delegate = [WKAppManager sharedInstance].currentNavigationController.childViewControllers.lastObject;
	[[WKAppManager sharedInstance].currentNavigationController pushViewController:vc animated:YES];
}

- (void)func_openScan:(id)params {
	[[NSNotificationCenter defaultCenter] postNotificationName:@"open_scan" object:nil];
}


@end
