//
//  AppDelegate+util.m
//  WebViewController
//
//  Created by feidu_ios_02 on 2018/11/16.
//  Copyright © 2018 feidu. All rights reserved.
//

#import "AppDelegate+util.h"

@implementation AppDelegate (util)

NS_INLINE
NSString* version_path(){
	return [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/Version.data"];;
}

NS_INLINE
NSString* prev_version(){
	return [NSString stringWithContentsOfFile:version_path() encoding:NSUTF8StringEncoding error:nil];
}

static NSString* const CFBundleVersion =
@"CFBundleVersion";
NSString* current_version(){
	return [NSString stringWithFormat:@"%@",[NSBundle mainBundle].infoDictionary[CFBundleVersion]];
}

@end
