//
//  DCGMScanViewController.h
//  CDDStoreDemo
//
//  Created by 陈甸甸 on 2018/1/9.
//Copyright © 2018年 RocketsChen. All rights reserved.
//


#import <UIKit/UIKit.h>


@class DCGMScanViewController;
@protocol DCGMScanViewControllerDelegate <NSObject>

- (void)scanViewController:(DCGMScanViewController *)controller scanString:(NSString *)message;
@end

@interface DCGMScanViewController : UIViewController

@property (nonatomic, assign) id<DCGMScanViewControllerDelegate> delegate;

@end
